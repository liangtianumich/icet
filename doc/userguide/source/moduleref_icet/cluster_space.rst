.. index::
   single: Function reference; ClusterSpace
   single: Class reference; ClusterSpace

Cluster space
=============

.. module:: icet

.. autoclass:: ClusterSpace
   :members:
   :undoc-members:
   :inherited-members:

Supplementary functions
-----------------------
      
.. module:: icet.core.cluster_space

.. automethod:: icet.core.cluster_space.get_singlet_info

.. automethod:: icet.core.cluster_space.view_singlets
